// Fill out your copyright notice in the Description page of Project Settings.
#include "BullCowCartridge.h"

#include "HiddenWordList.h"

void UBullCowCartridge::BeginPlay() // When the game starts
{
    Super::BeginPlay();

    MinDifficulty = 3;
    MaxDifficulty = 7;
    Difficulty = MinDifficulty;

    IsogramMap = GetValidWords(Words);

    SetupGame();
}

void UBullCowCartridge::OnInput(const FString& Input) // When the player hits enter
{
    if (bGameOver == true) 
    {
        ClearScreen();
        SetupGame();
    }
    else
    {
        ProcessGuess(Input);
    }
}

void UBullCowCartridge::SetupGame()
{
    // Setup game variables
    int32 WordCount = IsogramMap[Difficulty].Num();
    HiddenWord = IsogramMap[Difficulty][FMath::RandRange(0, WordCount - 1)];
    Lives = HiddenWord.Len() * 3;
    bGameOver = false;

    // Welcome the player
    PrintLine(TEXT("Welcome to Bull Cows!"));
    PrintLine(TEXT("Guess the %i letter word!"), HiddenWord.Len());
    PrintLine(TEXT("You have %i lives."), Lives);
}

void UBullCowCartridge::ProcessGuess(const FString& Guess)
{
    if (Guess == HiddenWord)
    {
        PrintLine(TEXT("You have Won!"));
        Difficulty = FMath::Clamp<int32>(Difficulty + 1, MinDifficulty, MaxDifficulty);
        EndGame();
        return;
    }

    if (Guess.Len() != HiddenWord.Len())
    {
        PrintLine(TEXT("You must enter a %i letter word."), HiddenWord.Len());
        return;
    }

    if (IsIsogram(Guess) == false)
    {
        PrintLine(TEXT("You must enter an isogram"));
        return;
    }

    if (--Lives <= 0) 
    {
        PrintLine(TEXT("You have Lost!"));
        PrintLine(TEXT("The hidden word was %s"), *HiddenWord);
        Difficulty = MinDifficulty;
        EndGame();
        return;
    }

    int32 BullCount, CowCount;
    GetBullCows(Guess, BullCount, CowCount);
    PrintLine(TEXT("You got %i Bulls and %i Cows"), BullCount, CowCount);

    PrintLine(TEXT("%i lives remaining"), Lives);
}

bool UBullCowCartridge::IsIsogram(const FString& Word) const
{
    for (int32 PivotIndex = 0; PivotIndex < Word.Len(); ++PivotIndex)
    {
        for (int32 CompareIndex = PivotIndex + 1; CompareIndex < Word.Len(); ++CompareIndex)
        {
            if (Word[PivotIndex] == Word[CompareIndex])
            {
                return false;
            }
        }
    }

    return true;
}

TMap<int32, TArray<FString>> UBullCowCartridge::GetValidWords(const TArray<FString>& WordList) const
{
    TMap<int32, TArray<FString>> ValidWordMap;
    for (FString Word : WordList)
    {
        if ((Word.Len() >= MinDifficulty && Word.Len() <= MaxDifficulty) && IsIsogram(Word))
        {
            if (ValidWordMap.Contains(Word.Len()) == false)
            {
                ValidWordMap.Add(Word.Len(), TArray<FString>());
            }
            ValidWordMap[Word.Len()].Emplace(Word);
        }
    }

    return ValidWordMap;
}

void UBullCowCartridge::GetBullCows(const FString& Guess, int32& BullCount, int32& CowCount) const 
{
    BullCount = 0;
    CowCount = 0;
    for (int32 GuessIndex = 0; GuessIndex < Guess.Len(); GuessIndex++)
    {
        if (Guess[GuessIndex] == HiddenWord[GuessIndex]) 
        {
            BullCount++;
            continue;
        }

        int32 SearchIndex;
        if (HiddenWord.FindChar(Guess[GuessIndex], SearchIndex))
        {
            CowCount++;
            continue;
        }
    }
}

void UBullCowCartridge::EndGame()
{
    bGameOver = true;
    PrintLine(TEXT("Game over! Press enter to continue..."));
}